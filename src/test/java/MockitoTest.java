import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class MockitoTest {

    @Test
    public void testConverterService() {
        SizeCalculatorService s = new SizeCalculatorService();

        assertEquals(20.0, s.convertSize(40), 0.000001);
    }

    @Test
    public void testMockitoConverterService() {
        SizeCalculatorService s = mock(SizeCalculatorService.class);

        when(s.convertSize(40)).thenReturn(20.0);
        when(s.convertSize(50)).thenReturn(30.0);
        when(s.convertSize(60)).thenReturn(40.0);

        assertEquals(20.0, s.convertSize(40), 0.000001);
        assertEquals(20.0, s.convertSize(40), 0.000001);
      //  assertEquals(30.0, s.convertSize(50), 0.000001);
       // assertEquals(40.0, s.convertSize(60), 0.000001);

        verify(s, times(1)).convertSize(40);
    }
}
