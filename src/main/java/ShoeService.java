public class ShoeService {

    private ShoeRepository repo;

    public ShoeService(ShoeRepository repo) {
        this.repo = repo;
    }

    public Shoe getShoe(double size) {
        return repo.get(size);
    }
}
