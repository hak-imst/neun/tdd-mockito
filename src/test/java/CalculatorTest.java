import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    Calculator c = null;

    CalculatorService service = Mockito.mock(CalculatorService.class);

    @BeforeEach
    public void setUp() {
        c = new Calculator(service);
    }

    @Test
    void testAdd() {
        when(service.perform(2,3)).thenReturn(5);
        when(service.perform(-10,5)).thenReturn(-5);

        assertEquals(-2, c.add(2, 3));
        assertEquals(-2, c.add(-10, 5));
        assertEquals(13, c.add(10, 5));

        assertNotEquals(0, c.add(2, 3));

        verify(service, times(2)).perform(2,3);
        verify(service).perform(-10,5);
    }

    @Test
    void testComplex() {
        try {
            c.complex("d", 0);
            fail();
        } catch (NumberFormatException e) {
            // richtig
        }

        assertThrows(NumberFormatException.class, () -> {
            c.complex("a", 0);
        });

        assertDoesNotThrow(() -> {
            c.complex("4", 0);
        });
    }
}
