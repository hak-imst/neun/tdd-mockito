public class Shoe {

    private double size;

    public Shoe() {
    }

    public Shoe(double size) {
        this.size = size;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) throws InvalidShoeSizeException {
        if (size > 0) {
            this.size = size;
        } else {
            throw new InvalidShoeSizeException();
        }
    }

}
