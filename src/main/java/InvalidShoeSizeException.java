public class InvalidShoeSizeException extends Exception {
    public InvalidShoeSizeException() {
        super("Shoesize has to be > 0");
    }
}
