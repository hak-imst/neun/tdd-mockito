import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ShoeTest {

    private Shoe s;

    @BeforeEach
    void setUp() {
        s = new Shoe(40);
    }

    @Test
    void getSize() {
        assertEquals(40, s.getSize());
    }

    @Test
    void setSizeOk() {
        try {
            assertEquals(40, s.getSize());
            assertNotEquals(43, s.getSize());
            s.setSize(43);
            assertEquals(43, s.getSize());
        } catch (InvalidShoeSizeException e) {
            fail();
        }
    }

    @Test
    void setSizeFail() {
        try {
            s.setSize(-43);
            fail();
        } catch (InvalidShoeSizeException e) {
            assertEquals(40, s.getSize());
        }
    }

    @Test
    void setSizeOkNotThrows() {
        assertDoesNotThrow(() -> {
            s.setSize(43);
        });
    }

    @Test
    void setSizeFailThrows() {
        assertThrows(InvalidShoeSizeException.class, () -> {
            s.setSize(-43);
        });
    }

}
