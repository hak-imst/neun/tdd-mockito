public class Calculator {

    private CalculatorService service;

    public Calculator(CalculatorService service) {
        this.service = service;
    }

    public int add(int a, int b) {

        int ergebnis = service.perform(a, b) + 2;

        return a + b - ergebnis;
    }

    public int complex(String a, int b) throws NumberFormatException {

        int c = Integer.parseInt(a);

        return 0;
    }
}
