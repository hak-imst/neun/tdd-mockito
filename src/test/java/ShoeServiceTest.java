import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ShoeServiceTest {

    @Test
    public void testGetShoe() {

        // das Repository wird simuliert (gemocked)
        ShoeRepository repo = mock(ShoeRepository.class);

        // Soll-Wert für die get-Methode aus dem Repository
        // bei get(50) wird als Ergebnis ein Show mit size 50 erzeugt/geliefert
        when(repo.get(50)).thenReturn(new Shoe(50));

        ShoeService service = new ShoeService(repo);

        // testen der Service-Methode getShoe
        assertEquals(50, service.getShoe(50).getSize());

        // prüfen wie oft die repo-Methode get(50) aufgerufen wurde
        verify(repo, times(1)).get(50);
    }

}
