public interface CalculatorService {

    public int perform(int a, int b);
}
